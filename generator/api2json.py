import os,sys,json, requests, datetime, pytz, time
from jsoncal import constructJson
from lxml import etree
from io import StringIO

debugtoggle = False
serieslist = ["F1","F2","F3","Supercup"]
cutoffyear = 2020
circuitbanlist = [
    "Kyalami Grand Prix Circuit","Autódromo Internacional do Rio de Janeiro","Detroit Street Circuit","Adelaide Street Circuit","Autódromo José Carlos Pace","Circuito Permanente del Jarama","Montjuïc Circuit","Circuit de Pedralbes","Zeltweg Airfield","Mosport International Raceway","Mont-Tremblant","Aintree Racecourse","Circuit de Nevers Magny-Cours","Circuit Dijon-Prenois","Circuit de Reims-Gueux","Charade Circuit","Rouen les Essarts","Bugatti Au Mans","Automobil-Verkehrs und Übungs-Straße","Complexe Européen de Nivelles-Baulers","Phoenix Street Circuit","Dallas Fair Park","Sebring Raceway","Riverside International Raceway","Mount Fuji","Prince George Circuit","Autódromo do Estoril","Monsanto Park","Circuit da Boavista","Autódromo Juan y Oscar Gálvez","Scandinavian Raceway","Dijon-Prenois","Bremgarten","Ain-Diab Circuit","Pescara Circuit","Long Beach Street Circuit","Detroit","Caesars Palace","Yas Marina Circuit","Brands Hatch Circuit","Donington Park","Circuito Permanente de Jerez","Watkins Glen International","TI Circuit Aida"]

def clear(printstr:str=None):
    if not debugtoggle: os.system('cls')
    if printstr != None:
        print(printstr)
def getIsoDateinStringbyOffset(isodate:str, offset:int):
    date = datetime.date.fromisoformat(isodate)
    newDate = date + datetime.timedelta(offset)
    return newDate.strftime('%Y-%m-%d')

class f1tv():
    def __init__(self):
        self.json = constructJson("f1tv.json")
    def complete_url(self,endpoint:str,api_version:int=2):
        if api_version == 2: complete_url = 'https://f1tv-api.formula1.com/agl/1.0/gbr/en/all_devices/global/' + endpoint
        else: complete_url = 'https://f1tv.formula1.com/api/' + endpoint
        return complete_url
    def canonical_url(self,url:str):
        canonical_url = url.replace('https://f1tv-api.formula1.com/agl/1.0/gbr/en/all_devices/global/','')
        return canonical_url
    def convert_path(self,relpath:str,folder:str):
        cwd = os.getcwd()
        div = os.path.sep
        path = cwd+div+folder+relpath.replace("/","_")
        return path
    def parse(self,prefix:str,jsoninput,outfile):
        if(isinstance(jsoninput, dict)):
            for entry in jsoninput:
                self.parse(prefix+str(entry)+":",jsoninput[entry],outfile)
        elif(isinstance(jsoninput, list)):
            for entry in jsoninput:
                self.parse("\t"+prefix,entry,outfile)
        else:
            outfile.write(str(prefix+str(jsoninput)).replace("\t","")+"\r")
    def request_to_dict(self,url:str):
        #print("requesting "+url,end=".... ")
        time.sleep(1)
        r = requests.get(url)
        #print("done")
        if(r.status_code//100 == 2):
            jsondict = json.loads(r.text)
            return jsondict
    def getCircuits(self):
        return self.request_to_dict(self.complete_url("circuit",api_version=1))
    def getCircuit(self,circuit_id:str):
        return self.request_to_dict(self.complete_url("circuit/"+circuit_id,api_version=1))
    def getEvent(self,event_id:str):
        return self.request_to_dict(self.complete_url("event-occurrence/"+event_id))
    def getSession(self,session_id:str):
        return self.request_to_dict(self.complete_url("session-occurrence/"+session_id))
    def getCircuitIdDict(self):
        print("getCircuitIdDict")
        outDict = {}
        circuitDict = self.getCircuits()
        for circuit in circuitDict['objects']:
            if datetime.datetime.fromisoformat(circuit['last_data_ingest']).replace(tzinfo=pytz.UTC) < datetime.datetime.now().replace(tzinfo=pytz.UTC) - datetime.timedelta(days=2):
                print("Nothing new here, exiting.")
                os.remove("f1tv.json")
                sys.exit()
            if circuit['name'] in circuitbanlist: continue
            outDict.setdefault(circuit['name'],circuit['uid'])
        return outDict
    def getEventIdDict(self):
        print("getEventIdDict")
        outDict = {}
        CircuitIdDict = self.getCircuitIdDict()
        for circuit in CircuitIdDict:
            circuitDict = self.getCircuit(CircuitIdDict[circuit])
            print(circuit)
            outDict.setdefault(circuit,circuitDict['eventoccurrence_urls'])
            #break ###
        return outDict
    def getSessionIdDict(self):
        print("getSessionIdDict")
        outDict = {}
        EventIdDict = self.getEventIdDict()
        for circuit in EventIdDict:
            outDict.setdefault(circuit,{})
            for event in EventIdDict[circuit]:
                eventDict = self.getEvent(event.split("/")[3])
                year = eventDict['end_date'].split("-")[0]
                if int(year) < cutoffyear: continue
                eventname = eventDict['name']
                print(year,eventname)
                outDict     [circuit]                                                       .setdefault(   year         ,               {}              )
                outDict     [circuit][year]                                                 .setdefault(   "F1"         ,               {}              )
                outDict     [circuit][year]['F1']                                           .setdefault( eventname      ,               {}              )
                outDict     [circuit][year]['F1'][eventname]                                .setdefault( 'Sessions'     ,               {}              )
                for session in eventDict['sessionoccurrence_urls']:
                    outDict [circuit][year]['F1'][eventname]['Sessions']                    .setdefault(session['name'] ,               {}              )
                    outDict [circuit][year]['F1'][eventname]['Sessions'][session['name']]   .setdefault(  'session_id'  , session['self'].split("/")[3] )
                    outDict [circuit][year]['F1'][eventname]['Sessions'][session['name']]   .setdefault(    'Start'     ,     eventDict['end_date']     )
                    outDict [circuit][year]['F1'][eventname]['Sessions'][session['name']]   .setdefault(     'End'      ,               ''              )
                #break ###
        return outDict
    def writeDataDict(self):
        print("getDataDict")
        SessionIdDict = self.getSessionIdDict()
        outDict = SessionIdDict
        for             circuit in SessionIdDict:
            for         year    in SessionIdDict[circuit]:
                for     event   in SessionIdDict[circuit][year]['F1']:
                    for session in SessionIdDict[circuit][year]['F1'][event]['Sessions']:
                        sessionurl=SessionIdDict[circuit][year]['F1'][event]['Sessions'][session]['session_id']
                        sessionDict = self.getSession(sessionurl)
                        #print (session)
                        if sessionDict['start_time'] == None or sessionDict['end_time'] == None:
                            if "R" in session:
                                outDict[circuit][year]['F1'][event]['Sessions'][session].pop('session_id')
                            elif "Q" in session or "3" in session:
                                outDict[circuit][year]['F1'][event]['Sessions'][session].pop('session_id')
                                outDict[circuit][year]['F1'][event]['Sessions'][session]['Start'] = getIsoDateinStringbyOffset(outDict[circuit][year]['F1'][event]['Sessions'][session]['Start'],-1)
                            elif "P" in session :
                                outDict[circuit][year]['F1'][event]['Sessions'][session].pop('session_id')
                                outDict[circuit][year]['F1'][event]['Sessions'][session]['Start'] = getIsoDateinStringbyOffset(outDict[circuit][year]['F1'][event]['Sessions'][session]['Start'],-2)
                        else:
                            outDict[circuit][year]['F1'][event]['Sessions'][session] = {'Start':sessionDict['start_time'],'End':sessionDict['end_time']}
                        for seri in serieslist:
                            if seri in session: serie=seri ;break
                            else: serie="F1"
                        self.json.createEvent(circuit,year,serie,event)
                        self.json.createSession(circuit,year,serie,event,session,outDict[circuit][year]['F1'][event]['Sessions'][session]['Start'],outDict[circuit][year]['F1'][event]['Sessions'][session]['End'])
                    self.json.writeJson
        #self.json.json_obj = outDict
        return outDict

class motogp():
    def __init__(self):
        print("init motogp",end="...")
        self.json = constructJson("motogp.json")
        self.parser = etree.HTMLParser()
        print("done")
    def getPage(self,url):
        time.sleep(2)
        page = requests.get(url)
        try:
            html = page.content.decode("utf-8")
        except:
            html = page.text
        return html
    def getHTML(self,page):
        return etree.parse(StringIO(page), parser=self.parser)
    def getEventList(self,baseURL,linkURL):
        tree=self.getHTML(self.getPage(baseURL))        #https://stackoverflow.com/q/51788359/
        refs = tree.xpath("//a")                        #get <a> Tags
        links = [link.get('href', '') for link in refs] #get URL
        return [l for l in links if linkURL in l]       #get event URLS
    def getSchedule(self,eventURL):
        tree=self.getHTML(self.getPage(eventURL))
        schedule = tree.xpath("//div[@class='c-schedule__table-row ']")
        schedOut = []
        for el in schedule:
            start = el[0][0].attrib['data-ini-time']
            try:
                end = el[0][1].attrib['data-end']
            except:
                end = self.approxRaceEnd(start)
            series = el[1].text
            session = el[2][0].text
            schedOut.append({'Series':series,'Session':session,'Start':start,'End':end})
        return schedOut  
    def writeJson(self):
        print("fetching calendar page")
        events = self.getEventList('https://www.motogp.com/en/calendar','https://www.motogp.com/en/event')
        events = list(set(events))
        print("..done.")
        print("fetching events")
        for eventurl in events:
            # print("get "+event+" schedule",end="...")
            eventdata = self.getSchedule(eventurl)
            # print("done")
            # print("prepare "+event+" schedulejson",end="...")
            try:
                event,circuit,country = self.getCircuit(eventurl)
            except:
                print("skipping "+eventurl)
                continue
            event = eventurl.split("motogp.com/en/event/")[1].replace("+"," ")
            print(event+"...",end="")
            carry = None
            for eventdict in eventdata:
                year = eventdict['Start'][:4]
                self.json.createEvent(circuit,year,eventdict['Series'],event)
                if eventdict['Session'] == "Qualifying Nr. 1 ":
                    carry = (circuit,year,eventdict['Series'],event,"Qualifying",self.fixTZ(eventdict['Start']))
                    continue
                if eventdict['Session'] == "Warm Up" or "Press" in eventdict['Session'] or "After The Flag" in eventdict['Session'] or "photo" in eventdict['Session']: continue
                if carry != None:
                    self.json.createSession(*carry,self.fixTZ(eventdict['End']))
                    carry = None
                    continue
                self.json.createSession(circuit,year,eventdict['Series'],event,eventdict['Session'],self.fixTZ(eventdict['Start']),self.fixTZ(eventdict['End']))
            # print("done")

            self.json.writeJson
        print("..done.")
    def fixTZ(self,dt):
        output = dt[:22]+":"+dt[-2:]
        return output
    def approxRaceEnd(self,dt):
        output = dt[:11]+   str('00'+str(int(dt[11:13])+1))[-2:]        +dt[-11:]
        return output # "2021-04-18T11:20:00+01:00"      
    def getCircuit(self,eventURL):
        tree=self.getHTML(self.getPage(eventURL))
        circuitdata = tree.xpath("//div[@class='c-event__block c-event__circuit-data']")
        event = circuitdata[0][1][0][1][0].text
        circuit = circuitdata[0][1][0][1][1].text
        country = circuitdata[0][1][0][1][2].text
        return event,circuit,country



        # print(schedule)

motogp().writeJson()
#f1tv().writeDataDict()
