from jsoncal import constructJson
import json, os, sys, traceback, templates, shutil, builtins as __builtin__
from datetime import date, datetime, time, timedelta
from colorama import Fore, Back, Style

## input json file to modify
json=constructJson(input("json filename: "))
debugtoggle = True
prettyPrintAutoBreak = True
prettyPrintColor = True

## Wrapper for toggling traceback
def customerr(output:str):
    clear(output)
    if debugtoggle: traceback.print_exc()
## Clear the screen and optionally print a Title line
def clear(printstr:str=None):
    if not debugtoggle: os.system('cls')
    if printstr != None:
        print(printstr)
## Custom Sort Key for Sorting Venues by Country, Place, Name
def venueSort(venue:str) -> str:
    if "," in venue:
        return " ".join(venue.split(",")[::-1])
    return venue
## redefine print, adding autocolor
def print(string:str, colorMode=None, end=None):
    invert = Back.WHITE + Fore.BLACK
    reset = Fore.RESET + Back.RESET
    if prettyPrintColor:
        if colorMode == "Venue List":
            lst = string.split(",")
            __builtin__.print( invert + lst[0] + reset + "," + ", ".join( lst[1::1] ) , end=end )
    ###   If no special: just print
        else: __builtin__.print(string, end=end)
    else: __builtin__.print(string, end=end)
## display list of dict keys nicely readable
def prettyprintkeylist(list:list, breakCount:int=None,prefix:str=""):
    termwidth = shutil.get_terminal_size((80,20))[0]
    colCount = 0
    if list[0] == "Country": list = list.pop(0)
    clear(prefix)
    for i in range(len(list)):
        if colCount+len(list[i])+10 >= termwidth: print("") ; colCount = 0
        red = Fore.BLACK+Back.RED
        reset = Fore.RESET+Back.RESET
        print( red + "[" + str(i) , end= "]" + reset + ": ")
        print(list[i],end="   ", colorMode="Venue List")
        colCount += len(list[i])+10
        if (breakCount != None):
            if (i%breakCount == breakCount-1 and i != 0 and not prettyPrintAutoBreak ):
                print("")
    print("")
    return list
## return value of key
def unkeylist(list:list, input:str):
    if (str(input).isdigit()):
        return list[int(input)]
    else:
        return input
## if one choice, return choice immediately, otherwise let choose
def getinput(prompt:str, list:list=[]):
    if len(list) == 1 :
        return 0
    else:
        inp = input(prompt)
        if inp == "":
            return None
        return inp
### ### INPUTS ### ###
## Input Venue
def inVenue(jsonC):
    keysvenue = list(jsonC.json_obj['Venues'].keys())
    for i in range(len(keysvenue)): keysvenue[i] = keysvenue[i]+", "+jsonC.json_obj['Venues'][keysvenue[i]]['Country']
    keysvenue.sort(key=venueSort)
    keysvenue=prettyprintkeylist(keysvenue,3,"Select Venue:")
    venue = getinput("Venue: ",keysvenue)
    if venue == None:
        raise IndexError
    return unkeylist(keysvenue,venue)
## Input Year
def inYear(jsonC,venue):
    keylist = list(jsonC.json_obj['Venues'][venue].keys())
    while 'Country' in keylist: keylist.remove('Country')
    keysyear=prettyprintkeylist(list(keylist),3,venue)
    year = getinput("Year: ",keysyear)
    if year == None:
        raise IndexError
    return unkeylist(keysyear,year)
## Input Series
def inSeries(jsonC,venue,year):
    keysseries=prettyprintkeylist(list(jsonC.json_obj['Venues'][venue][year].keys()),3,venue+" "+year)
    series = getinput("Series: ",keysseries)
    if series == None:
        raise IndexError
    return unkeylist(keysseries,series)
## Input Event
def inEvent(jsonC,venue,year,series):
    keysevent=prettyprintkeylist(list(jsonC.json_obj['Venues'][venue][year][series].keys()),3,venue+" "+year+" "+series)
    event = getinput("Event: ",keysevent)
    if event == None:
        raise IndexError
    return unkeylist(keysevent,event)
### ### WIZARDS ### ###
## assist in populating the json with a new season all at once, before any session is known
def createNewSeason(jsonC):
    try:
        year = input("Year: ")
        series = input("Series: ")
        while(True):
            venue = inVenue(jsonC)
            event = getinput("Event: ")
            if venue == None or event == None:
                break
            if venue.count(',') == 2: venue = venue.rsplit(', ',1)[0]
            jsonC.createEvent(venue,year,series,event)
    except IndexError:
        customerr("Please take a valid choice.")
    except:
        customerr("Unkown Error.")  
## create new sessions inside an already set up season
def createNewSessions(jsonC):
    try:
        while(True):
            ### Input Venue
            venue = inVenue(jsonC)
            if venue.count(',') == 2: venue = venue.rsplit(', ',1)[0]
            ### Input Year/Season
            year = inYear(jsonC,venue)
            ### Input Series
            series=inSeries(jsonC,venue,year)
            ### Input Event
            event = inEvent(jsonC,venue,year,series)
            ### Summarize selection
            clear(venue+" "+year+" "+series+" "+event)
            while(True):
                sessionName = input("Sessionname: ")
                sessionStart = input("Datetime Start: ")
                sessionEnd = input("Datetime End: ")
                if sessionName == "" or sessionStart == "":
                    break
                jsonC.createSession(venue,year,series,event,sessionName,sessionStart,sessionEnd)    
    except IndexError:
        customerr("Please take a valid choice.")
    except:
        customerr("Unkown Error.")     
## Apply a predefined Session Structure onto a Race Weekend
def applyTemplate(jsonC):
    try:
        while(True):
            ### Input Venue
            venue = inVenue(jsonC)
            if venue.count(',') == 2: venue = venue.rsplit(', ',1)[0]
            ### Input Year/Season
            year = inYear(jsonC,venue)
            ### Input Series
            series=inSeries(jsonC,venue,year)
            ### Input Event
            event = inEvent(jsonC,venue,year,series)
            ### Summarize selection
            clear(venue+" "+year+" "+series+" "+event)
            ### do basic validity check on date format before continuing
            while True: 
                racedate = input("Race Day [yyyy-MM-dd]: ")
                if racedate.replace("-","").isdigit():
                    racedate = tuple(racedate.split("-"))
                    break
                else:
                    clear(venue+" "+year+" "+series+" "+event)
            ### Apply Template to Event
            if jsonC.json_obj['Venues'][venue][year][series][event]['Sessions'] == {}:
                session = getattr(templates,"announcement")(date(*(int(x) for x in racedate)))
            else:
                session = getattr(templates,series.lower().replace(" ",""))(date(*(int(x) for x in racedate))) ## select template depending on series and input date as datetime.date
            session.setSessions() ## ask for all the starttimes
            for sessionelement in session.listdictSessions(): ## for every sessiontemplateobject create session in json
                if sessionelement['end'] == None:
                    jsonC.createSession(venue,year,series,event,sessionelement['name'],sessionelement['start'].strftime("%Y-%m-%d"),"")    
                else:
                    jsonC.createSession(venue,year,series,event,sessionelement['name'],sessionelement['start'].strftime("%Y%m%dT%H%M%SZ%z"),sessionelement['end'].strftime("%Y%m%dT%H%M%SZ%z"))    
    except IndexError:
        customerr("Please take a valid choice.")
    except:
        customerr("Unkown Error.")
## Change Start and End of a Session
def rescheduleSession(jsonC):
    try:
        while(True):
            ### Input Venue
            venue = inVenue(jsonC)
            if venue.count(',') == 2: venue = venue.rsplit(', ',1)[0]
            ### Input Year/Season
            year = inYear(jsonC,venue)
            ### Input Series
            series=inSeries(jsonC,venue,year)
            ### Input Event
            event = inEvent(jsonC,venue,year,series)
            ### Summarize selection
            clear(venue+" "+year+" "+series+" "+event)
            while True:
                ### Input Session
                keyssession=prettyprintkeylist(list(jsonC.json_obj['Venues'][venue][year][series][event]['Sessions'].keys()),3,venue+" "+year+" "+series+" "+event)
                session = getinput("Session: ",keyssession)
                if session == None:
                    break
                session = unkeylist(keyssession,session)
                clear(venue+" "+year+" "+series+" "+event+" "+session)
                try:
                    sessionStart = input("Start: "+str(jsonC.json_obj['Venues'][venue][year][series][event]['Sessions'][session]['Start'])+" - New value: ")
                    sessionEnd = input("End: "+str(jsonC.json_obj['Venues'][venue][year][series][event]['Sessions'][session]['End'])+" - New value: ")
                except KeyboardInterrupt:
                    break
                if sessionStart == "":
                    break
                jsonC.changeSession(venue,year,series,event,session,sessionStart,sessionEnd)
    except IndexError:
        customerr("Please take a valid choice.")
    except:
        customerr("Unkown Error.")
## Change name of a Session
def renameSession(jsonC):
    try:
        while(True):
            ### Input Venue
            venue = inVenue(jsonC)
            if venue.count(',') == 2: venue = venue.rsplit(', ',1)[0]
            ### Input Year/Season
            year = inYear(jsonC,venue)
            ### Input Series
            series=inSeries(jsonC,venue,year)
            ### Input Event
            event = inEvent(jsonC,venue,year,series)
            ### Summarize selection
            clear(venue+" "+year+" "+series+" "+event)
            while True:
                ### Input Session
                keyssession=prettyprintkeylist(list(jsonC.json_obj['Venues'][venue][year][series][event]['Sessions'].keys()),3,venue+" "+year+" "+series+" "+event)
                session = getinput("Session: ",keyssession)
                if session == None:
                    break
                session = unkeylist(keyssession,session)
                clear(venue+" "+year+" "+series+" "+event+" "+session)
                try:
                    newSessionName = input("Sessionname: "+str(session)+" - New value: ")
                except KeyboardInterrupt:
                    break
                if newSessionName == "":
                    break
                jsonC.renameSession(venue,year,series,event,session,newSessionName)
    except IndexError:
        customerr("Please take a valid choice.")
    except:
        customerr("Unkown Error.")
### ### MENU ### ###
while(True):
    clear("1: Define Season   2: Custom Session   3: Reschedule Session   4: Rename Session   5: Apply Template   any: exit")
    call = input("Choice:  ")
    if call == "1":
        createNewSeason(json)
    elif call == "2":
        createNewSessions(json)
    elif call == "3":
        rescheduleSession(json)
    elif call == "4":
        renameSession(json)
    elif call == "5":
        applyTemplate(json)
    else:
        break