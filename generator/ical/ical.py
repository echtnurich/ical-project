from datetime import datetime
from hashlib import blake2b
### Class iCal
# 
class ical():
    ## init
    # Set inital variables and open .ics file to write to
    def __init__(self,organization:str,icspath:str):
        self.organization = organization
        self.file = open(icspath,"w+")
        self.startFile()
    ## write event
    # takes template and optional arguments
    # if arguments are given, a template containing {1},{2}-Style placeholders are expected
    # give sufficient arguments to fill all placeholders
    def writeOut(self,template,*args):
        if(args != None):
            template = template.format(*args)
            for line in template.split('§'):
                #print(line)
                self.file.write(line)
                self.file.write("\r")
        else:
            for line in template.split('§'):
                #print(line)
                self.file.write(line)
                self.file.write("\r")
    ## file header
    def startFile(self):
        fileheader = 'BEGIN:VCALENDAR§VERSION:2.0§PRODID:{0}§METHOD:PUBLISH§X-WR-CALNAME:{1}§X-WR-CALDESC:{2}'
        self.writeOut(fileheader,"-//"+self.organization+"//iCal Project//EN",self.organization,self.organization)
    # make sogo like my calendar entries :'(
    def genUID(self, string:str):
        # 0641020d-6190-4c16-8320-62e5a931dc3d
        # 2fa86b7c-d235-2e98-339f-8a1730e06366
        # 2fa86b7-d235-e983-9f8a-730e0636
        hash = blake2b(digest_size=16)
        hash.update(bytes(string, 'utf-8'))
        #print(hash.hexdigest())
        return hash.hexdigest()[0:8]+"-"+hash.hexdigest()[8:13]+"-"+hash.hexdigest()[13:18]+"-"+hash.hexdigest()[18:23]+"-"+hash.hexdigest()[23:31]
    ## adjust event parameters to make the calendar entries look nice
    # then write it
    def writeEvent(self,_series,_season,_venue,_event,_session,_start,_end):
        _dtstamp = datetime.utcnow().strftime("%Y%m%dT%H%M%SZ%z")
        _uid = self.genUID(_series+_season+_event+_session+"@"+self.organization)
        _desc = _series+" "+_event+" | "+_session

        #_uid = _uid.replace(" ","-").replace("/","-").replace(",","-").replace("--","-").replace("--","-").replace("--","-").replace("--","-")

        event = 'BEGIN:VEVENT§UID:{0}§DTSTAMP:{1}§CATEGORIES:{2}§CLASS:PUBLIC§LOCATION:{8}§DESCRIPTION:{3}§DTSTART:{4}§DTEND:{5}§ORGANIZER:{6}§SUMMARY:{7}§END:VEVENT'
        if ( "VALUE=DATE:" in _start and "VALUE=DATE:" in _end): # if time given in dates, use different template
            event = 'BEGIN:VEVENT§UID:{0}§DTSTAMP:{1}§CATEGORIES:{2}§CLASS:PUBLIC§LOCATION:{8}§DESCRIPTION:{3}§DTSTART;{4}§DTEND;{5}§ORGANIZER:{6}§SUMMARY:{7}§END:VEVENT'
        self.writeOut(event,_uid,_dtstamp,_series,_desc,_start,_end,self.organization,_desc,_venue)
    ## file footer
    def endFile(self):
        filefooter='END:VCALENDAR'
        self.writeOut(filefooter)