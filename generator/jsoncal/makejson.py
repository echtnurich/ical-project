import json, os, sys

## class with many methods specifically adjusted for the json structure expected
class constructJson():
    def __init__(self,_filename):
        self.filename = _filename
        try:
            self.json_obj = json.load(open(self.filename,"r"))
        except:
            with open(self.filename,"w+") as self.jsonfile:
                self.json_obj = {}
                self.json_obj['Venues'] = {}
                json.dump(self.json_obj, self.jsonfile)
        finally:
            self.file = open(self.filename,"r+")
            self.json_obj = json.load(self.file)
        pass
    def createVenue(self,venueName:str):
        self.json_obj['Venues'].setdefault(venueName, {})
        self.json_obj['Venues'][venueName].setdefault('Country', "")
        self.writeJson()

    def createEvent(self,venueName,eventYear,eventSeries,eventName):
        self.createVenue(venueName)
        self.json_obj['Venues'][venueName].setdefault(eventYear, {})
        self.json_obj['Venues'][venueName][eventYear].setdefault(eventSeries, {})
        self.json_obj['Venues'][venueName][eventYear][eventSeries].setdefault(eventName, {})
        self.json_obj['Venues'][venueName][eventYear][eventSeries][eventName].setdefault('Sessions', {})
        self.writeJson()

    def createSession(self,venueName,eventYear,eventSeries,eventName,sessionName,sessionStart,sessionEnd):
        self.json_obj['Venues'][venueName][eventYear][eventSeries][eventName]['Sessions'][sessionName] = {}
        self.json_obj['Venues'][venueName][eventYear][eventSeries][eventName]['Sessions'][sessionName].setdefault('Start', sessionStart)
        self.json_obj['Venues'][venueName][eventYear][eventSeries][eventName]['Sessions'][sessionName].setdefault('End', sessionEnd)
        self.writeJson()

    def changeEvent(self,venueName,eventYear,eventSeries,eventName,newEventName):
        self.json_obj['Venues'][venueName][eventYear][eventSeries][newEventName] = self.json_obj['Venues'][venueName][eventYear][eventSeries][eventName]
        self.json_obj['Venues'][venueName][eventYear][eventSeries].pop(eventName)
        self.writeJson()

    def changeSession(self,venueName,eventYear,eventSeries,eventName,sessionName,sessionStart,sessionEnd):
        self.json_obj['Venues'][venueName][eventYear][eventSeries][eventName]['Sessions'][sessionName]['Start'] = sessionStart
        self.json_obj['Venues'][venueName][eventYear][eventSeries][eventName]['Sessions'][sessionName]['End'] = sessionEnd
        self.writeJson()

    def renameSession(self,venueName,eventYear,eventSeries,eventName,sessionName,newSessionName):
        self.json_obj['Venues'][venueName][eventYear][eventSeries][eventName]['Sessions'][newSessionName] = self.json_obj['Venues'][venueName][eventYear][eventSeries][eventName]['Sessions'][sessionName]
        self.json_obj['Venues'][venueName][eventYear][eventSeries][eventName]['Sessions'].pop(sessionName)
        self.writeJson()

    def deleteEvent(self,venueName,eventYear,eventSeries,eventName):
        self.json_obj['Venues'][venueName][eventYear][eventSeries].pop(eventName)
        self.writeJson()

    def deleteSession(self,venueName,eventYear,eventSeries,eventName,sessionName,sessionStart,sessionEnd):
        self.json_obj['Venues'][venueName][eventYear][eventSeries][eventName]['Sessions'][sessionName]['Start'] = sessionStart
        self.json_obj['Venues'][venueName][eventYear][eventSeries][eventName]['Sessions'][sessionName]['End'] = sessionEnd
        self.writeJson()

    def writeJson(self):
        self.closeFile()
        with open(self.filename,"w+") as self.jsonfile:
            json.dump(self.json_obj, self.jsonfile, indent=4)
        self.file = open(self.filename,"r+")
        self.json_obj = json.load(self.file)

    def closeFile(self):
        self.file.close()
    ## a small debug shell to call these methods directy inside a debugger as x._____________()
    def shell(self):
        x=constructJson(input("Filename: "))
        while(True):
            try:
                print(eval(input("> ")))
            except:
                print(sys.exc_info())
                pass