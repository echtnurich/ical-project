from ical import ical
import json,os,sys,datetime,pytz

## read JSON file
def openJson(jsonfilename:str):
    jfile = open(jsonfilename,"r+")
    jsonfile = json.load( jfile ) 
    name = jsonfilename.split(".")[0]
    return jsonfile, name
## get list of Series in JSON file
def getSeries(jsonfile:dict):
    returnSeries = ()
    for venue in jsonfile['Venues']:
        for season in jsonfile['Venues'][venue]:
            if ( season == "Country"): continue
            for series in jsonfile['Venues'][venue][season]:
                returnSeries += (series,)
    return list(set(returnSeries))
## get list of Seasons in JSON file
def getSeason(jsonfile:dict):
    returnSeason = ()
    for venue in jsonfile['Venues']:
        for season in jsonfile['Venues'][venue]:
            if ( season == "Country"): continue
            returnSeason += (season,)
    return list(set(returnSeason))
### MAIN ###
## parse json file to get all sessions and write the sessions to the ics file
def jsontoics(jsonfile:dict, name:str , seriesArg:str = None , seasonArg:str = None ):
    if ( seriesArg != None ): name = name+ "_" + seriesArg
    if ( seasonArg != None ): name = name+ "_" + seasonArg
    orgsuffix = " ".join(name.split("_")[1:])
    x = ical("echtnurich.de: "+orgsuffix,name+".ics")
    for venue in jsonfile['Venues']:
        for season in jsonfile['Venues'][venue]:
            if ( seasonArg != None ): season = seasonArg
            if ( seasonArg not in jsonfile['Venues'][venue] and seasonArg != None): continue
            if ( season == "Country"): continue
            for series in jsonfile['Venues'][venue][season]:
                if ( seriesArg != series and seriesArg != None): continue
                #if ( seriesArg != None ): series = seriesArg
                if ( seriesArg not in jsonfile['Venues'][venue][season] and seriesArg != None ): continue
                for event in jsonfile['Venues'][venue][season][series]:
                    for session in jsonfile['Venues'][venue][season][series][event]['Sessions']:
                        if ( jsonfile['Venues'][venue][season][series][event]['Sessions'][session]['Start'] != '' and jsonfile['Venues'][venue][season][series][event]['Sessions'][session]['End'] != ''):
                            sessionStart    = datetime.datetime.fromisoformat(jsonfile['Venues'][venue][season][series][event]['Sessions'][session][ 'Start'  ]).astimezone(pytz.UTC).strftime("%Y%m%dT%H%M%SZ")
                            sessionEnd      = datetime.datetime.fromisoformat(jsonfile['Venues'][venue][season][series][event]['Sessions'][session][   'End'  ]).astimezone(pytz.UTC).strftime("%Y%m%dT%H%M%SZ")
                        elif ( ":" not in jsonfile['Venues'][venue][season][series][event]['Sessions'][session]['Start']):
                            sessionStart = "VALUE=DATE:{}".format(datetime.datetime.fromisoformat(jsonfile['Venues'][venue][season][series][event]['Sessions'][session]['Start']).strftime("%Y%m%d"))
                            sessionEnd = "VALUE=DATE:{}".format((datetime.datetime.fromisoformat(jsonfile['Venues'][venue][season][series][event]['Sessions'][session]['Start']) + datetime.timedelta(days=1)).strftime("%Y%m%d"))
                        x.writeEvent(series,season,venue,event,session,sessionStart,sessionEnd)
                if ( seriesArg != None ): continue
            if ( seasonArg != None ): continue
    x.endFile()
## to that for all json files
for file in os.listdir():
    if ".json" in file:
        jsonfile , name = openJson(file)
        jsontoics( jsonfile , name )
        seriesfor = list([None]) + getSeries( jsonfile )
        seasonfor = list([None]) + getSeason( jsonfile )
        for series in seriesfor:
            for season in seasonfor:
                if ( season == "Country"): continue
                jsontoics( jsonfile , name , series , season )
