from datetime import date, datetime, time, timedelta
import inspect

class event():
    ### Init with basic data common for every session in this event
    ## ## This should be more or less the same for the whole season of a series, make changes for exceptions if needed
    def __init__(self, raceday:date):
        self.sessions= []
        self.raceday = raceday
        self.getSessions()
    ### Create a session object of a subclass of session YOU have defined elsewhere!
    def newSession(self,sessionclass:str, start:time) -> object:
        return getattr(self,sessionclass)(self.raceday, start)
    ### Property all defined sessions. When subclassing, all sessionclasses should be in this list!
    def getSessions(self):
        self._sessionclasses = []
        for classattribute in dir(self):
            if "_" in classattribute: continue # no internal shit
            #elif classattribute == "session": continue # no template session
            elif inspect.isclass(getattr(self,classattribute)) and issubclass(getattr(self,classattribute), self.session) and getattr(self,classattribute) != self.session:
                self._sessionclasses.append(classattribute)
    #---- sessionclasses = property(getSessions) ---# for some reason this recurses to death...
    ### Define all sessions that 1. are in list sessionclasses and 2. are defined as subclasses of session
    def setSessions(self) -> list:
        sessions = []
        for session in self._sessionclasses:
            starttime = input("Start time for session "+session+" hh:mm : ").split(":")
            if starttime[0] == "":
                sessions.append(self.newSession(session, None))
            else:
                sessions.append(self.newSession(session, time(int(starttime[0]),int(starttime[1]))))
        self.sessions = sessions
        return sessions
    ### Print out all Sessions defined
    def printSessions(self, sessions:list=None):
        if sessions == None: sessions = self.sessions
        for session in sessions:
            print(session.name)
            print(session.start)
            print(session.end)
    ### Return Dict for use in ezjson
    def listdictSessions(self, sessions:list=None):
        if sessions == None: sessions = self.sessions
        sessionList = []
        for session in sessions:
            sessionList.append({'name': session.name, 'start': session.start, 'end':session.end})
        return sessionList
    ### Master template for a racing session
    class session():
        ### Make Name, Start, End from: Raceday, Starttime, Duration, Sessionname, Dateoffset(0: raceday, -1: qualiday etcpp...)
        def __init__(self, raceday:date, start:time):
            self._start = start
            self.set()
            self.sessionoffset = timedelta(days=self.offset)
            self._name = self.sessionname
            if self.start == None:
                self._start = raceday + self.sessionoffset
            else:
                self._start = datetime.combine(raceday,start) + self.sessionoffset
            if self.duration == None:
                self._end = None
            else:
                self._end   = self.start + self.duration
        ### Redefine this when subclassing
        def set(self):
            self.duration = timedelta(hours=1)
            self.sessionname = "Session"
            self.offset = 0
            
        ### Properties
        def getName(self):
            return self._name
        name = property(getName)

        def getStart(self):
            return self._start
        start = property(getStart)

        def getEnd(self):
            return self._end
        end = property(getEnd)
