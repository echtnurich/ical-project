import mastertemplate, os,traceback, inspect
from datetime import date, datetime, time, timedelta

class f1(mastertemplate.event):
    class race(mastertemplate.event.session):
        def set(self):
            self.duration = timedelta(hours=2)
            self.sessionname = "Race"
            self.offset = 0
    class qualifying(mastertemplate.event.session):
        def set(self):
            self.duration = timedelta(hours=1)
            self.sessionname = "Qualifying"
            self.offset = -1
    class practice3(mastertemplate.event.session):
        def set(self):
            self.duration = timedelta(hours=1)
            self.sessionname = "Free Practice 3"
            self.offset = -1
    class practice2(mastertemplate.event.session):
        def set(self):
            self.duration = timedelta(hours=1)
            self.sessionname = "Free Practice 2"
            self.offset = -2
    class practice1(mastertemplate.event.session):
        def set(self):
            self.duration = timedelta(hours=1)
            self.sessionname = "Free Practice 1"
            self.offset = -2

##example, do this elsewhere!
#test= f1(date(2021,1,1))
#os.system('cls')
#test.setSessions()
#test.printSessions()
#while(True):
#    try:
#        print(eval(input("> ")))
#    except:
#        traceback.print_exc()