import json, os, sys, pytz
from datetime import datetime, timedelta

jsonfiles = os.listdir(sys.path[0])
jsonfiles = os.listdir('.')

def openJson(jsonfilename:str):
    jfile = open(jsonfilename,"r+")
    jsonfile = json.load( jfile ) 
    jfile.close()
    return jsonfile

def getEventDict():
    eventDict = {}
    for jsonfile in jsonfiles:
        if '.json' in jsonfile:
            jsonC=openJson(jsonfile)
            for venue in jsonC['Venues']:
                for season in jsonC['Venues'][venue]:
                    for series in jsonC['Venues'][venue][season]:
                        if ( season == "Country"): continue
                        for event in jsonC['Venues'][venue][season][series]:
                            for session in jsonC['Venues'][venue][season][series][event]['Sessions']:
                                key = season+" "+series+" "+event+" - "+session
                                eventDict.setdefault(key,jsonC['Venues'][venue][season][series][event]['Sessions'][session]['Start'])
    return eventDict

def getNextWeek(jsonC):
    utcnow = datetime.utcnow()
    #utcnow = datetime(2021,6,11,12,0,0)
    utcnextweek = utcnow + timedelta(7)        #datetime.datetime.fromisoformat(circuit['last_data_ingest']).replace(tzinfo=pytz.UTC) < datetime.datetime.now().replace(tzinfo=pytz.UTC) - datetime.timedelta(days=2)
    outDict = {}
    for element in jsonC:           #"2020-07-31T10:00:00+00:00"
        # try:
        if datetime.fromisoformat(jsonC[element]).replace(tzinfo=pytz.UTC) > utcnow.replace(tzinfo=pytz.UTC) and datetime.fromisoformat(jsonC[element]).replace(tzinfo=pytz.UTC) < utcnextweek.replace(tzinfo=pytz.UTC):
            outDict.setdefault(element,"Local Time: "+datetime.fromisoformat(jsonC[element]).strftime("%Y-%m-%d %H:%M:%S")+"  -  UTC: "+datetime.fromisoformat(jsonC[element]).astimezone(pytz.UTC).strftime("%Y-%m-%d %H:%M:%S"))
        # except:
        #     pass
    return sorted(outDict.items(), key=lambda x: x[1])


original1 = """<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
    <title>iCal File Project</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <div class="navbar">
      <a href="https://ical.echtnurich.de/ics/">Manual Calendar</a>
      <a href="webcal://ical.echtnurich.de/ics/f1tv.ics">F1TV Calendar</a>
      <a href="webcal://ical.echtnurich.de/ics/motogp.ics">MotoGP Calendar</a>
      <a href="https://gitlab.com/echtnurich/ical-project">GitLab Project</a>
    </div>

    <h1>iCal Generator Project</h1>

    <p>
      The Motorsport calendars published here are automatically generated from json. <br>
      All tools are selfwritten and published on my GitLab.
    </p>
    <h2>Upcoming events:</h2>
"""
original2 = """
  </body>
</html>"""
with open(sys.path[0]+os.sep+"index.html","w+") as htmlfile:
    htmlfile.write(original1)
    htmlfile.write("<table style=\"text-align: left\">")
    resultDict = getNextWeek(getEventDict())
    for element in resultDict:
        htmlfile.write("<tr>")
        htmlfile.write("<th>"+element[0]+"</th><td>"+element[1]+"</td>")
        htmlfile.write("</tr>")
    if len(resultDict) == 0:
        htmlfile.write("<tr><th>No race sessions scheduled within the next week.</th></tr>")
    htmlfile.write("</table>")
    htmlfile.write(original2)