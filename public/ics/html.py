import os,sys,json
icsfiles = os.listdir(sys.path[0])
icsfiles.sort()
categories = json.load ( open( sys.path[0]+os.sep+"categories.json" ) )
circuses = json.load ( open( sys.path[0]+os.sep+"circuses.json" ) )
linkprefix= "webcal://ical.echtnurich.de/ics/"

def addStyle(htmlfile):
        htmlfile.write("""
        <style>
            table, th, td {
                border: 1px solid black;
                vertical-align: bottom;
                padding: 2px;
            }
            table {
                margin-top: 10px;
                margin-bottom: 10px;
                margin-left: 0; 
                margin-right: 0;
            }
            td {
                text-align: center
            }
            div {
                margin: 0 auto;
                display: table;
            }
            a {
                color: black
            }
        </style>
        """)

def genTable():
    tableDict = {}
    for file in icsfiles:
        if ".ics" in file and "main" in file:
            filelst = file.replace(".ics","").split("_")
            if len(filelst) == 1: fileclean = file.replace(".ics","").replace(filelst[0]+"_", "").replace("_"," ").title()
            else: fileclean = file.replace(".ics","").replace(filelst[0]+"_", "").replace("_"," ")
            if (len(filelst) == 1):
                tableDict.setdefault(filelst[0],{})
                tableDict[filelst[0]]['a'] = "<a href=\""+linkprefix+file+"\">"+fileclean+"</a>"
            if (len(filelst) == 2):
                tableDict.setdefault(filelst[0],{})
                tableDict[filelst[0]].setdefault(filelst[1],{})
                tableDict[filelst[0]][filelst[1]]['a'] = "<a href=\""+linkprefix+file+"\">"+fileclean+"</a>"
            if (len(filelst) == 3):
                tableDict.setdefault(filelst[0],{})
                tableDict[filelst[0]].setdefault(filelst[1],{})
                tableDict[filelst[0]][filelst[1]].setdefault(filelst[2],{})
                tableDict[filelst[0]][filelst[1]][filelst[2]]['a'] = "<a href=\""+linkprefix+file+"\">"+fileclean+"</a>"
    return tableDict
            
def turntable(dicttable:dict):
    table_row_cell = {}
    for calendar in dicttable:
        #print(calendar)
        #print(dicttable[calendar]['a'])

        table_row_cell.setdefault(calendar,{})
        table_row_cell[calendar].setdefault('allseasons',{})
        table_row_cell[calendar]['allseasons']['a'] = dicttable[calendar]['a']

        for first in dicttable[calendar]:
            if (first == 'a'):continue
            #print(first)
            #print(dicttable[calendar][first]['a'])

            if (first.isdigit()):
                table_row_cell[calendar].setdefault(first,{})
                table_row_cell[calendar][first]['a'] = dicttable[calendar][first]['a']
            else:
                table_row_cell[calendar]['allseasons'].setdefault(first,{})
                table_row_cell[calendar]['allseasons'][first]['a'] = dicttable[calendar][first]['a']

            for second in dicttable[calendar][first]:
                if (second == 'a'):continue
                #print(second)
                #print(dicttable[calendar][first][second]['a'])

                table_row_cell[calendar][second].setdefault(first,{})
                table_row_cell[calendar][second][first]['a'] = dicttable[calendar][first][second]['a']
    return table_row_cell

def tr(content):
    return "<tr>"+"".join(content)+"</tr>"
def td(content):
    return "<td>"+"".join(content)+"</td>"
def th(content, option=None):
    if option != None: return "<th "+option+">"+"".join(content)+"</th>"
    return "<th>"+"".join(content)+"</th>"

def writeTable(htmlfile, htmltable):
    htmlfile.write("<div>")
    for calendar in htmltable:
        htmlfile.write("<table>")
        titlerow = [th(htmltable[calendar]['allseasons']['a'])]
        for x in htmltable[calendar]['allseasons']:
            if(x!='a'):
                titlerow.append( th(htmltable[calendar]['allseasons'][x]['a']) )
        htmlfile.write(tr(titlerow))

        for season in htmltable[calendar]:
            if (season.isdigit()):
                row = [th(htmltable[calendar][season]['a'])]
                for x in htmltable[calendar][season]:
                    if (x != 'a'):
                        row.append( td(htmltable[calendar][season][x]['a']) )
                htmlfile.write(tr(row))
        htmlfile.write("</table>")
    htmlfile.write("</div>")

def writeCategories(htmlfile, htmltable, catJson:dict):
    htmlfile.write("<div>")
    for cat in catJson:
        htmlfile.write("<table style=\"display: inline-block;\">")
        htmlfile.write( tr( th( cat , "colspan = "+str(len(htmltable['main']['allseasons'])-1) ) ) )
        for season in htmltable['main']:
            if (season.isdigit()):
                row = [th(str(season))]
                for x in htmltable['main'][season]:
                    if (x != 'a'):
                        if x not in catJson[cat]: continue
                        row.append( td(htmltable['main'][season][x]['a']) )
                if len(row) == 1: continue
                htmlfile.write(tr(row))
        htmlfile.write("</table>")
    htmlfile.write("</div>")    

def writeCircus(htmlfile, htmltable, circJson:dict):
    htmlfile.write("<div>")
    for cYear in circJson:
        for circus in circJson[cYear]:
            htmlfile.write("<table style=\"display: inline-block;\">")
            htmlfile.write( tr( th( circus , "colspan = "+str(len(htmltable['main']['allseasons'])-1) ) ) )
            for season in htmltable['main']:
                if cYear != season: continue
                if (season.isdigit()):
                    row = [th(str(season))]
                    for x in htmltable['main'][season]:
                        if (x != 'a'):
                            if x not in circJson[cYear][circus]: continue
                            row.append( td(htmltable['main'][season][x]['a']) )
                    if len(row) == 1: continue
                    htmlfile.write(tr(row))
            htmlfile.write("</table>")
    htmlfile.write("</div>") 

with open(sys.path[0]+os.path.sep+"index.html","w+") as htmlfile:
    addStyle(htmlfile)
    writeTable(htmlfile,turntable(genTable()))
    htmlfile.write("<hr>")
    writeCategories(htmlfile,turntable(genTable()),categories)
    htmlfile.write("<hr>")
    writeCircus(htmlfile,turntable(genTable()),circuses)

